package pl.edu.umk.mat.androidcourse.mastodonclient

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MessageViewModel : ViewModel() {
    val liveStatus: MutableLiveData<StatusDatatype?> = MutableLiveData(null)

    fun fillMessage(db: MastodonStatusDatabase, id: Long) {
        println("fillMessage(db=$db, id=$id)")

        if (liveStatus.value?.id == id) {
            println("fillMessage: found existing message with id=$id")
            // there is no need to access the database, use what is already here
            return
        }

        // database access should be run in a separate thread (one for I/O)
        viewModelScope.launch(Dispatchers.IO) {
            val dbDao = db.mastodonStatusDao()
            val status = dbDao.getStatusById(id)

            // Posts a task to a *main* thread to set the given value.
            // NOTE: this is called from IO thread, not from main thread
            liveStatus.postValue(status)

            println("fillMessage: postValue($status)")
        }
    }
}