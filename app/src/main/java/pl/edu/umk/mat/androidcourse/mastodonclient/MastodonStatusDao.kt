package pl.edu.umk.mat.androidcourse.mastodonclient

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MastodonStatusDao {
    @Query("SELECT * FROM MastodonStatus ORDER BY id DESC")
    fun getAll(): List<MastodonStatus>

    @Query("SELECT MAX(id) as maxId FROM MastodonStatus")
    fun getMaxId(): Long

    @Query("SELECT * FROM MastodonStatus WHERE id = :id")
    fun getStatusById(id: Long): MastodonStatus

    @Insert
    fun insertAll(vararg statuses: MastodonStatus)

    @Insert
    fun insertAll(statuses: List<MastodonStatus>)

    @Delete
    fun delete(status: MastodonStatus)
}