package pl.edu.umk.mat.androidcourse.mastodonclient

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.sys1yagi.mastodon4j.api.entity.Status

// see https://developer.android.com/guide/topics/ui/layout/recyclerview#Adapter
class StatusAdapter(private val dataset: MutableList<StatusDatatype>) :
    RecyclerView.Adapter<StatusAdapter.StatusViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is composed of three lines w/ Avatar
    // all held in LinearLayout (which is root View / ViewGroup).
    class StatusViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    interface Listener {
        fun onClick(position: Int)
    }
    private var listener : Listener? = null
    fun setListener(listener: Listener) {
        this.listener = listener
    }

    /**
     * Called when RecyclerView needs a new [ViewHolder] of the given type to represent
     * an item.
     *
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     *
     * The new ViewHolder will be used to display items of the adapter using
     * [.onBindViewHolder]. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary [View.findViewById] calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     * an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see .getItemViewType
     * @see .onBindViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatusViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_view_item, parent, false)

        return StatusViewHolder(view)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the [ViewHolder.itemView] to reflect the item at the given
     * position.
     *
     * Note that unlike [android.widget.ListView], RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the `position` parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use [ViewHolder.getBindingAdapterPosition] which
     * will have the updated adapter position.
     *
     * Override [.onBindViewHolder] instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     * item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: StatusViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val view: View = holder.view

        val text1 : TextView = view.findViewById(R.id.textView1)
        val text2 : TextView = view.findViewById(R.id.textView2)
        val text3 : TextView = view.findViewById(R.id.textView3)
        val imgView : ImageView = view.findViewById(R.id.imgView)

        text1.text = HtmlCompat.fromHtml(dataset[position].content, HtmlCompat.FROM_HTML_MODE_LEGACY)
        text2.text =
            "Favorited: "+dataset[position].favouritesCount.toString()+", "+
            "Reblogged: "+dataset[position].reblogsCount.toString()
        text3.text =
            "Language: "+dataset[position].language+", "+
            "Visibility: "+dataset[position].visibility

        Picasso
            .get()
            .load(dataset[position].avatar)
            .into(imgView)

        view.setOnClickListener {
            listener?.let {
                it.onClick(position)
            }
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount(): Int = dataset.size
}
