package pl.edu.umk.mat.androidcourse.mastodonclient

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.WorkInfo

class MainActivity : AppCompatActivity() {

    // source of data for displaying stream of messages (statuses)
    private val model: StreamViewModel by viewModels()

    // see https://developer.android.com/guide/topics/ui/layout/recyclerview
    private lateinit var recyclerView: RecyclerView

    private lateinit var swipeLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val contents = model.contents

        recyclerView = findViewById<RecyclerView>(R.id.recyclerViewStream).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // position the items in the list vertically
            layoutManager = LinearLayoutManager(this@MainActivity)

            // set adapter for elements of recycler view
            val viewAdapter = StatusAdapter(contents)
            viewAdapter.setListener(listener = object : StatusAdapter.Listener {
                override fun onClick(position: Int) {
                    println("MainActivity: onClick(position = $position) for item $position/${contents.size} in recycler view")

                    val intent = Intent(this@MainActivity, DisplayMessageActivity::class.java)
                    intent.putExtra(EXTRA_STATUS_ID, contents[position].id)
                    startActivity(intent)
                }
            })
            adapter = viewAdapter
        }

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        swipeLayout = findViewById<SwipeRefreshLayout>(R.id.swipeRefreshStream).apply {
            this.setOnRefreshListener {
                // This method performs the actual data-refresh operation.
                updateTimeline()
            }
        }


        // refresh RecyclerView whenever StreamViewModel changes
        model.liveContents.observe(this) {
            recyclerView.adapter?.notifyDataSetChanged()
            swipeLayout?.setRefreshing(false)
        }

        updateTimeline()
    }

    /**  Called by system to create options menu at first use */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    /** Handle click events for options menu */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.menu_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.menu_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.menu_refresh -> {
                // Signal SwipeRefreshLayout to start the progress indicator
                swipeLayout?.isRefreshing = true
                // Start the refresh background task.
                updateTimeline()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun updateTimeline() {
        // queue downloading stream from Mastodon server into database
        UpdateWorker.enqueueActionUpdate(this, "home")
        // update UI from values stored in database
        model.updateContents(MastodonStatusDatabase.getInstance(this))
    }

    /** Called when the user taps the Send button */
    fun sendMessage(view: View) {
        val editText = findViewById<EditText>(R.id.editText)
        val message = editText.text.toString()

        // if there is aything to send
        if (message.isEmpty()) return
        // enqueue sending message to Mastodon server,
        // and post a toast after this work succeeds
        UpdateWorker
            .enqueueActionSend(this, message)
            .observe(this) { workInfo ->
                if (workInfo?.state == WorkInfo.State.SUCCEEDED) {
                    Toast.makeText(this,
                        resources.getString(R.string.toast_sent),
                        Toast.LENGTH_SHORT).show()
                }
            }
        // clear the message to prepare for sending next message
        editText.text.clear()
    }
}