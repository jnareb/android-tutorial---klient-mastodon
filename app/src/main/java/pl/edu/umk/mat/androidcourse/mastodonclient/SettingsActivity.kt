package pl.edu.umk.mat.androidcourse.mastodonclient

import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.EditTextPreference
import androidx.preference.PreferenceFragmentCompat

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat(),
                             SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            // Adjust preferences and their UI settings
            val hostnamePref = findPreference<EditTextPreference>("hostname_preference")
            hostnamePref?.setDefaultValue(BuildConfig.INSTANCE_NAME)

            val accessTokenPref = findPreference<EditTextPreference>("access_token_preference")
            accessTokenPref?.setDefaultValue(BuildConfig.ACCESS_TOKEN)
            if (accessTokenPref != null && accessTokenPref.text == null)
                accessTokenPref.text = BuildConfig.ACCESS_TOKEN
            // EditTextPreference from androidx.preference doesn't have getEditText()
            accessTokenPref?.setOnBindEditTextListener {
                it.inputType = InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            }
        }

        /**
         * Called when a shared preference is changed, added, or removed.
         *
         * This may be called even if a preference is set to its existing value.
         * May not be called when preferences are cleared (unless targetin Android R or later).
         * This callback will be run on your main thread.
         */
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?,
                                               key: String?) {
            println("onSharedPreferenceChanged: $key")
            if (key == "hostname_preference" ||
                key == "access_token_preference")
                UpdateWorker.invalidateClientInstance()
        }

        /**
         * Called when the fragment is visible to the user and actively running.
         * This is generally tied to [Activity.onResume]
         * of the containing Activity's lifecycle.
         */
        override fun onResume() {
            super.onResume()
            preferenceScreen.sharedPreferences
                ?.registerOnSharedPreferenceChangeListener(this)
        }

        /**
         * Called when the Fragment is no longer resumed.
         * This is generally tied to [Activity.onPause]
         * of the containing Activity's lifecycle.
         */
        override fun onPause() {
            super.onPause()
            preferenceScreen.sharedPreferences
                ?.unregisterOnSharedPreferenceChangeListener(this)
        }
    }
}