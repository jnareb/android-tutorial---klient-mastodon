package pl.edu.umk.mat.androidcourse.mastodonclient

import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.preference.PreferenceManager
import androidx.work.*
import com.google.gson.Gson
import com.sys1yagi.mastodon4j.MastodonClient
import com.sys1yagi.mastodon4j.api.Pageable
import com.sys1yagi.mastodon4j.api.Range
import com.sys1yagi.mastodon4j.api.entity.Status
import com.sys1yagi.mastodon4j.api.exception.Mastodon4jRequestException
import com.sys1yagi.mastodon4j.api.method.Public
import com.sys1yagi.mastodon4j.api.method.Statuses
import com.sys1yagi.mastodon4j.api.method.Timelines
import okhttp3.OkHttpClient
import java.net.HttpURLConnection
import java.net.UnknownHostException

// parameters for workers (actual action was used in IntentService)
const val EXTRA_ACTION = "pl.edu.umk.mat.androidcourse.mastodonclient.extra.ACTION"

// actions Worker can perform, e.g. ACTION_FETCH_NEW_ITEMS
const val ACTION_SEND   = "pl.edu.umk.mat.androidcourse.mastodonclient.action.SEND"
const val ACTION_UPDATE = "pl.edu.umk.mat.androidcourse.mastodonclient.action.UPDATE"

// parameters for sending messages
const val EXTRA_MESSAGE = "pl.edu.umk.mat.androidcourse.mastodonclient.MESSAGE"
// parameters for updating stream
const val EXTRA_STREAM_TYPE = "pl.edu.umk.mat.androidcourse.mastodonclient.STREAM_TYPE"

class UpdateWorker(appContext: Context, workerParams: WorkerParameters):
    Worker(appContext, workerParams) {

    private var client: MastodonClient? = null

    /**
     * Override this method to do your actual background processing.  This method is called on a
     * background thread - you are required to **synchronously** do your work and return the
     * [androidx.work.ListenableWorker.Result] from this method.  Once you return from this
     * method, the Worker is considered to have finished what its doing and will be destroyed.  If
     * you need to do your work asynchronously on a thread of your own choice, see
     * [ListenableWorker] or [CoroutineWorker].
     *
     *
     * A Worker has a well defined
     * [execution window](https://d.android.com/reference/android/app/job/JobScheduler)
     * to finish its execution and return a [androidx.work.ListenableWorker.Result].  After
     * this time has expired, the Worker will be signalled to stop.
     *
     * @return The [androidx.work.ListenableWorker.Result] of the computation; note that
     * dependent work will not execute if you use
     * [androidx.work.ListenableWorker.Result.failure] or
     * [androidx.work.ListenableWorker.Result.retry]
     */
    override fun doWork(): Result {
        val action =
            inputData.getString(EXTRA_ACTION) ?: return Result.failure()

        println("UpdateWorker: action $action")

        if (client == null) {
            println("- getting MastodonClient instance...")
            client = getClientInstance(applicationContext)
        } else {
            println("- reusing existing MastodonClient")
        }

        when (action) {
            ACTION_SEND -> {
                println("- getting SEND inputData: $EXTRA_MESSAGE")
                val msg =
                    inputData.getString(EXTRA_MESSAGE) ?: return Result.failure()
                println("UpdateWorker: sending '$msg'")
                return if (handleActionSend(msg))
                    Result.success() else Result.failure()
            }
            ACTION_UPDATE -> {
                println("- getting UPDATE inputData: $EXTRA_STREAM_TYPE")
                val streamType =
                    inputData.getString(EXTRA_STREAM_TYPE) ?: return Result.failure()
                println("UpdateWorker: downloading $streamType stream")
                return if (handleActionUpdate(streamType))
                    Result.success() else Result.failure()
            }
        }

        // Indicate whether the work finished successfully with the Result
        return Result.success()
    }

    private fun handleActionSend(message: String): Boolean {
        println("UpdateWorker: SEND $message")

        if (client == null) {
            println("!!! client is null")
            return false
        }
        if (message.isEmpty()) {
            // trying to send an empty message results in HTTP 422 Unprocessable Entity error
            println("!!! message to send is empty")
            return false
        }

        try {
            val status = Statuses(client!!)
            status.postStatus(
                message,
                null, null, false, null,
                Status.Visibility.Private
            ).execute()
        } catch (e: Mastodon4jRequestException) {
            handleException(e)
            return false
        } catch (e: Exception) {
            println("!!! error=$e")
            return false
        }
        return true
    }

    private fun handleActionUpdate(streamType: String): Boolean {
        println("UpdateWorker: UPDATE $streamType")
        val db = MastodonStatusDatabase.getInstance(applicationContext)
        println("- with database: $db")
        val dbDao = db.mastodonStatusDao()

        val maxId = dbDao.getMaxId()
        println(">>> maxId = $maxId")

        if (client == null) {
            println("!!! client is null")
            return false
        }
        println("- with Mastodon client: $client")

        // retrieve data from Mastodon
        val resultRange = Range(
            limit = 20,
            sinceId = maxId
        )
        val result: Pageable<Status>?
        try {
            result = if (streamType == "public") {
                val timelines = Public(client!!)
                timelines.getLocalPublic(resultRange).execute()
            } else {
                val timelines = Timelines(client!!)
                timelines.getHome(resultRange).execute()
            }
        } catch (e: Mastodon4jRequestException) {
            handleException(e)
            return false
        } catch (e: Exception) {
            println("!!! error=$e")
            return false
        }
        println(">>> downloaded ${result.part.size}")

        dbDao.insertAll(
            result.part.map { res ->
                MastodonStatus(
                    id = res.id,
                    content = res.content,
                    avatar = res.account?.avatar,
                    favouritesCount = res.favouritesCount,
                    reblogsCount = res.reblogsCount,
                    language = res.language,
                    visibility = res.visibility,
                    createdAt = res.createdAt
                )
            }
        )

        println("... inserted into database")
        return true
    }

    private fun handleException(e: Mastodon4jRequestException) {
        if (e.cause is UnknownHostException) {
            println("!!! error: unknown host")
            println(" - cause=${(e.cause as UnknownHostException).cause}")
            println(" - message=${(e.cause as UnknownHostException).message}")
            notifyUser("unknown host - check preferences")
        } else if (e.response?.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            println("!!! error: authorization failed")
            notifyUser("authentication failed - check preferences")
        } else {
            println("!!! error: message=${e.message}")
            println(" - response=${e.response}, cause=${e.cause}")
        }
    }

    private fun notifyUser(message: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            applicationContext.mainExecutor.execute {
                Toast.makeText(
                    applicationContext,
                    message,
                    Toast.LENGTH_LONG
                ).show()
            }
        } else {
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(
                    applicationContext,
                    message,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    companion object {
        // For Singleton instantiation of MastodonClient
        @Volatile
        private var clientInstance: MastodonClient? = null

        /**
         * Creates an instance of MastodonClient
         *
         * NOTE: the instance is stored in `clientInstance` private static field,
         * so it is kept for the lifetime longer than the lifetime of any Activity.
         * This means that the instance would be reused even if relevant preferences,
         * such as Mastodon server instance ("hostname_preference") and its access token
         * ("access_token_preference") were changed.
         * @see invalidateClientInstance
         *
         * @param context application context, to get shared preferences
         * @return MastodonClient instance
         */
        private fun getClientInstance(context: Context): MastodonClient? {
            return clientInstance ?: synchronized(this) {
                clientInstance ?: createMastodonClient(context).also { clientInstance = it }
            }
        }

        /**
         * Invalidates `UpdateWorker` instance of MastodonClient
         *
         * To be used when relevant user preferences (such as Mastodon server instance
         * ("hostname_preference") and its access token ("access_token_preference"))
         * were changed.
         */
        fun invalidateClientInstance() {
            clientInstance = null
        }

        private fun createMastodonClient(context: Context): MastodonClient? {
            println("- UpdateWorker: creating MastodonClient...")

            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val host  = prefs.getString("hostname_preference", BuildConfig.INSTANCE_NAME)
            val token = prefs.getString("access_token_preference", BuildConfig.ACCESS_TOKEN)

            val client = try {
                MastodonClient
                    .Builder(host!!, OkHttpClient.Builder(), Gson())
                    .accessToken(token!!)
                    .build()
            } catch (e: Mastodon4jRequestException) {
                println("! UpdateWorker: error creating MastodonClient: ${e.message}")
                e.response?.let {
                    when (val code = it.code()) {
                        HttpURLConnection.HTTP_UNAUTHORIZED,
                        HttpURLConnection.HTTP_FORBIDDEN -> {
                            println("! - [HTTP $code] wrong username or password for $host")
                        }
                        else -> {
                            println("! - [HTTP $code]: " + it.message())
                        }
                    }
                }
                null
            } catch (e: Throwable) {
                println("! UpdateWorker: unknown error creating MastodonClient: ${e.message}")
                null
            }

            return client
        }

        /**
         * Starts this worker to enqueue action Send with the given parameters.
         */
        @JvmStatic
        fun enqueueActionSend(context: Context, msg: String): LiveData<WorkInfo> {
            println("enqueueActionSend: msg=$msg")
            // use WorkManager instead of coroutines to send messages
            val workManager: WorkManager = WorkManager.getInstance(context)
            // expedited work requires much more work, because it may need to be done in foreground
            // see https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#expedited
            val sendMessageRequest = OneTimeWorkRequestBuilder<UpdateWorker>()
                //.setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .setInputData(workDataOf(
                    EXTRA_ACTION to ACTION_SEND,
                    EXTRA_MESSAGE to msg
                ))
                .addTag("UpdateWorker::ActionSend")
                .build()
            workManager.enqueue(sendMessageRequest)
            return workManager
                .getWorkInfoByIdLiveData(sendMessageRequest.id)
        }

        @JvmStatic
        fun enqueueActionUpdate(context: Context, streamType: String): LiveData<WorkInfo> {
            println("enqueueActionUpdate: streamType=$streamType")
            // use WorkManager instead of coroutines to get messages
            val workManager: WorkManager = WorkManager.getInstance(context)
            val updateRequest = OneTimeWorkRequestBuilder<UpdateWorker>()
                .setInputData(workDataOf(
                    EXTRA_ACTION to ACTION_UPDATE,
                    EXTRA_STREAM_TYPE to streamType,
                ))
                .addTag("UpdateWorker::ActionUpdate")
                .build()
            workManager.enqueue(updateRequest)
            return workManager
                .getWorkInfoByIdLiveData(updateRequest.id)
        }
    }
}