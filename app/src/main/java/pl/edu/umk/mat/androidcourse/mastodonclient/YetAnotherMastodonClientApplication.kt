package pl.edu.umk.mat.androidcourse.mastodonclient

import android.app.Application

class YetAnotherMastodonClientApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        println("onCreate application")
        MastodonStatusRepository.initialize(this)
    }
}