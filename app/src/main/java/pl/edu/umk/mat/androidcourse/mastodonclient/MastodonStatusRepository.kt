package pl.edu.umk.mat.androidcourse.mastodonclient

import android.content.Context
import androidx.room.Room

private const val DATABASE_NAME = "status-database"

class MastodonStatusRepository private constructor(context: Context) {

    // https://developer.android.com/training/data-storage/room#usage
    private val database: MastodonStatusDatabase = Room.databaseBuilder(
        context.applicationContext,
        MastodonStatusDatabase::class.java,
        DATABASE_NAME
    ).build()

    private val mastodonStatusDao = database.mastodonStatusDao()

    fun getStatuses(): List<MastodonStatus> = mastodonStatusDao.getAll()

    companion object {
        private var INSTANCE: MastodonStatusRepository? = null

        @Synchronized
        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = MastodonStatusRepository(context)
            }
        }

        fun get(): MastodonStatusRepository {
            return INSTANCE ?:
            throw IllegalStateException("MastodonStatusRepository must be initialized")
        }
    }
}