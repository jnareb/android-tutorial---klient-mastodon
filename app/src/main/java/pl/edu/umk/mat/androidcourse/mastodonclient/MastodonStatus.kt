package pl.edu.umk.mat.androidcourse.mastodonclient

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Room entity that represents those parts of Mastodon4j Status
 * that we want to store in the database and display.
 */
@Entity(tableName = "MastodonStatus")
data class MastodonStatus(
    @PrimaryKey val id: Long,
    val content: String,
    val avatar: String?,
    val favouritesCount: Int,
    val reblogsCount: Int,
    val language: String?,
    val visibility: String,
    val createdAt: String
)
