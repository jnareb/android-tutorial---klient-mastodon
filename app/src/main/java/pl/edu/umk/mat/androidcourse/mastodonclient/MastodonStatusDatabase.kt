package pl.edu.umk.mat.androidcourse.mastodonclient

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val DATABASE_NAME = "status-database"
private const val DATABASE_VERSION = 2

@Database(
    entities = [MastodonStatus::class],
    version = DATABASE_VERSION,
    //autoMigrations = [
    //    AutoMigration (from = 1, to = 2)
    //],
    exportSchema = true
)
abstract class MastodonStatusDatabase : RoomDatabase() {
    abstract fun mastodonStatusDao(): MastodonStatusDao

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: MastodonStatusDatabase? = null

        fun getInstance(context: Context): MastodonStatusDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): MastodonStatusDatabase {
            return Room
                .databaseBuilder(context, MastodonStatusDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}