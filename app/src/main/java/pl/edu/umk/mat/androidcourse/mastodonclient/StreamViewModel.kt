package pl.edu.umk.mat.androidcourse.mastodonclient

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import androidx.core.text.HtmlCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.sys1yagi.mastodon4j.MastodonClient
import com.sys1yagi.mastodon4j.api.Pageable
import com.sys1yagi.mastodon4j.api.Range
import com.sys1yagi.mastodon4j.api.entity.Status
import com.sys1yagi.mastodon4j.api.method.Public
import com.sys1yagi.mastodon4j.api.method.Timelines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient

typealias StatusDatatype = MastodonStatus

class StreamViewModel : ViewModel() {
    val contents: MutableList<StatusDatatype> = mutableListOf()
    val liveContents = MutableLiveData(contents)

    fun updateContents(db: MastodonStatusDatabase) {
        println("updateContents($db) ...")

        // database access should be run in a separate thread (one for I/O)
        viewModelScope.launch(Dispatchers.IO) {
            val dbDao = db.mastodonStatusDao()
            val newStatuses = dbDao.getAll()

            // replace old contents with new
            contents.clear()
            contents += newStatuses

            // Posts a task to a *main* thread to set the given value.
            // NOTE: this is called from IO thread, not from main thread
            liveContents.postValue(contents)

            println("... updated")
        }
    }
}