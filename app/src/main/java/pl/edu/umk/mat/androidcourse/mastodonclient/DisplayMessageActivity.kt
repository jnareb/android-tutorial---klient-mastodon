package pl.edu.umk.mat.androidcourse.mastodonclient

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.text.HtmlCompat
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.sys1yagi.mastodon4j.MastodonClient
import com.sys1yagi.mastodon4j.api.entity.Status
import com.sys1yagi.mastodon4j.api.method.Statuses
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient

const val EXTRA_STATUS_ID = "pl.edu.umk.mat.androidcourse.mastodonclient.extra.STATUS_ID"

class DisplayMessageActivity : AppCompatActivity() {

    // source of data for displaying stream of messages (statuses)
    private val model: MessageViewModel by viewModels()

    private lateinit var text1: TextView
    private lateinit var text2: TextView
    private lateinit var text3: TextView
    private lateinit var imgView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_message)

        // Get the Intent that started this activity and extract the status id
        val id = intent.getLongExtra(EXTRA_STATUS_ID, -1)
        println("DisplayMessageActivity::onCreate: id=$id")

        text1 = findViewById(R.id.textView1)
        text2 = findViewById(R.id.textView2)
        text3 = findViewById(R.id.textView3)
        imgView = findViewById(R.id.imgView)

        // use ViewModel to run database operations
        val db = MastodonStatusDatabase.getInstance(this)
        model.fillMessage(db, id)
        model.liveStatus.observe(this) { data ->
            data?.let { updateContents(it) }
        }
    }

    private fun updateContents(data: StatusDatatype) {
        println("DisplayMessageActivity::updateContents($data)")

        text1.text = HtmlCompat.fromHtml(data.content, HtmlCompat.FROM_HTML_MODE_LEGACY)
        text2.text =
            "Favorited: " + data.favouritesCount.toString() + ", " +
            "Reblogged: " + data.reblogsCount.toString()
        text3.text =
            "Language: " + data.language + ", " +
            "Visibility: " + data.visibility + "\n" +
            "Created: " + data.createdAt

        Picasso
            .get()
            .load(data.avatar)
            .into(imgView)
    }
}